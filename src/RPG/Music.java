package RPG;

public class Music {
	static String au1 = "src\\Music\\start.wav";
	static String au2 = "src\\Music\\开始背景.wav";
	static String au3 = "src\\Music\\存档.wav";
	static String au4 = "src\\Music\\电光一闪.wav";
	static String au5 = "src\\Music\\十万伏特.wav";
	static String au6 = "src\\Music\\治疗.wav";
	static String au7 = "src\\Music\\火花.wav";
	static String au8 = "src\\Music\\龙之怒.wav";
	static String au9 = "src\\Music\\急冻光线.wav";
	static String au10 = "src\\Music\\水之波动.wav";
	static String au11 = "src\\Music\\藤鞭.wav";
	static String au12 = "src\\Music\\阳光烈焰.wav";
	static String audio(int i) {
		if(i == 1) {
			return au1;
		}else if(i == 2) {
			return au2;
		}else if(i == 3) {
			return au3;
		}else if(i == 4) {
			return au4;
		}else if(i == 5) {
			return au5;
		}else if(i == 6) {
			return au6;
		}else if(i == 7) {
			return au7;
		}else if(i == 8) {
			return au8;
		}else if(i == 9) {
			return au9;
		}else if(i == 10) {
			return au10;
		}else if(i == 11) {
			return au11;
		}else if(i == 12) {
			return au12;
		}
		return null;
	}

}
