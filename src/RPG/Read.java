package RPG;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Read {
	int[] read = new int[6];
	public int[] read() {
		Connection c = null;
		Statement s = null;
		ResultSet rs = null;
		String sql = "select*from Pikachu";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:F:\\Pokemon.db");
			s = c.createStatement();
			rs = s.executeQuery(sql);
			while(rs.next()) {
				read[0] = (int) rs.getDouble("maxHp");
				read[1] = (int) rs.getDouble("maxMp");
				read[2] = rs.getInt("atk");
				read[3] = rs.getInt("def");
				read[4] = rs.getInt("lv");
				read[5] = rs.getInt("exp");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return read;
	}

}
