package RPG;

public class NPC1 extends Role {
	Au a = new Au();

	public NPC1() {
		super("喷火龙", 300, 300, 200,200, 100, 50,10, "src\\Image\\\u55B7\u706B\u9F99.png","火花","治疗", "龙之怒");
		// TODO 自动生成的构造函数存根
	}

	@Override
	public void attack(int skill, Role role) {
		// TODO 自动生成的方法存根

	}

	@Override
	public void skill1(Role role) {
		// TODO 自动生成的方法存根
		a.play(7);
		role.setLastHp(role.getHp());
		role.setHp(role.getHp()-this.getAtk()*0.8+role.getDef());

	}

	@Override
	public void skill2() {
		// TODO 自动生成的方法存根
		this.setLastMp(this.getMp());
		this.setMp(this.getMp()-0.3*this.getMaxMp());
		if(this.getMp() <=0) {
			this.setMp(this.getLastMp());
			StartFrame.flag = false;
			return;
		}else {
			a.play(6);
			this.setLastHp(this.getHp());
			this.setHp(this.getHp()+(this.getMaxHp()-this.getHp())*0.2);
		}

	}

	@Override
	public void skill3(Role role) {
		// TODO 自动生成的方法存根
		this.setLastMp(this.getMp());
		this.setMp(this.getMp()-0.4*this.getMaxMp());
		if(this.getMp() <=0) {
			this.setMp(this.getLastMp());
			StartFrame.flag = false;
			return;
		}else {
			a.play(8);
			role.setLastHp(role.getHp());
			if(role.getHp() <= 0.3*role.getHp()) {
				role.setHp(role.getHp()-this.getAtk()*1.5+role.getDef());
			}else {
				role.setHp(role.getHp()-this.getAtk()*1.2+role.getDef());	
			}
		}

	}

	@Override
	public void equipment(int i) {
		// TODO 自动生成的方法存根
		
	}

	@Override
	public void levelup() {
		// TODO 自动生成的方法存根
		
	}

	@Override
	public void removeEqu(int i) {
		// TODO 自动生成的方法存根
		
	}

}
