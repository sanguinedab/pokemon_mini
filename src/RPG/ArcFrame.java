package RPG;

import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class ArcFrame extends JFrame {
	Role hero;

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public ArcFrame(Role hero) {
		this.hero = hero;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(227, 390);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton button = new JButton();
		button.setIcon(new ImageIcon("src\\Image\\\u5B58\u6863.png"));
		button.setBorderPainted(false);
		button.setContentAreaFilled(false);
		button.setFocusPainted(false);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Au a = new Au();
				a.play(3);
				Archiving ar = new Archiving(hero.getMaxHp(),hero.getMaxMp(),hero.getAtk(),hero.getDef(),hero.getLv(),hero.getExp());
				ar.save();
				dispose();
			}
		});
		button.setBounds(62, 262, 93, 53);
		contentPane.add(button);
		
		JLabel lblNewLabel = new JLabel("最大生命值:"+hero.getMaxHp());
		lblNewLabel.setFont(new Font("黑体", Font.BOLD | Font.ITALIC, 12));
		lblNewLabel.setOpaque(true);
		lblNewLabel.setBounds(41, 20, 121, 15);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("最大法力值:"+hero.getMaxMp());
		lblNewLabel_1.setFont(new Font("黑体", Font.BOLD | Font.ITALIC, 12));
		lblNewLabel_1.setOpaque(true);
		lblNewLabel_1.setBounds(41, 45, 121, 15);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("攻击力:    "+hero.getAtk());
		lblNewLabel_2.setFont(new Font("黑体", Font.BOLD | Font.ITALIC, 12));
		lblNewLabel_2.setOpaque(true);
		lblNewLabel_2.setBounds(41, 70, 121, 15);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("防御力:    "+hero.getDef());
		lblNewLabel_3.setFont(new Font("黑体", Font.BOLD | Font.ITALIC, 12));
		lblNewLabel_3.setOpaque(true);
		lblNewLabel_3.setBounds(41, 95, 121, 15);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("等级:      "+hero.getLv());
		lblNewLabel_4.setFont(new Font("黑体", Font.BOLD | Font.ITALIC, 12));
		lblNewLabel_4.setOpaque(true);
		lblNewLabel_4.setBounds(41, 120, 121, 15);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("经验:      "+hero.getExp());
		lblNewLabel_5.setFont(new Font("黑体", Font.BOLD | Font.ITALIC, 12));
		lblNewLabel_5.setOpaque(true);
		lblNewLabel_5.setBounds(41, 145, 121, 15);
		contentPane.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel();
		lblNewLabel_6.setIcon(new ImageIcon("src\\Image\\\u7ED3\u7B97\u80CC\u666F.jpg"));
		lblNewLabel_6.setBounds(0, 0, 210, 350);
		contentPane.add(lblNewLabel_6);
	}
}
