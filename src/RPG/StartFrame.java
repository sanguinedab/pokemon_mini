package RPG;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JProgressBar;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.awt.event.ActionEvent;

/**
 * 
 * @author 蜡笔ab
 *
 */
public class StartFrame extends JFrame {
	Role role;
	Role role2;
	int k;
	static boolean flag;
	JProgressBar progressBar;
	JProgressBar progressBar_1;
	JProgressBar blueBar;
	JProgressBar blueBar_1;
	private Timer timer = new Timer();

	private JPanel contentPane;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_4;


	/**
	 * Create the frame.
	 */
	public StartFrame(Role role,Role role2) {
		this.role = role;
		this.role2 = role2;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(571, 345);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 555, 306);
		contentPane.add(panel);
		panel.setLayout(null);
		initBar();
		panel.add(progressBar);
		panel.add(progressBar_1);
		panel.add(blueBar);
		panel.add(blueBar_1);
		
		JButton btnNewButton = new JButton();
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				flag = true;
						try {
							attack_course(1);
						} catch (InterruptedException e) {
							// TODO 自动生成的 catch 块
							e.printStackTrace();
						}
			}
		});
		btnNewButton.setBounds(73, 255, 93, 23);
		btnNewButton.setText(role.getsName1());
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton();
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				flag = true;
						try {
							attack_course(2);
						} catch (InterruptedException e1) {
							// TODO 自动生成的 catch 块
							e1.printStackTrace();
						}
			}
		});
		btnNewButton_1.setBounds(176, 255, 93, 23);
		btnNewButton_1.setText(role.getsName2());
		panel.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton();
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				flag = true;
						try {
							attack_course(3);
						} catch (InterruptedException e1) {
							// TODO 自动生成的 catch 块
							e1.printStackTrace();
						}
			}
		});
		btnNewButton_2.setBounds(279, 255, 93, 23);
		btnNewButton_2.setText(role.getsName3());
		panel.add(btnNewButton_2);
		
		lblNewLabel = new JLabel();
		lblNewLabel.setIcon(new ImageIcon(role.getBodypath()));
		lblNewLabel.setBounds(89, 165, 80, 80);
		panel.add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel();
		lblNewLabel_1.setIcon(new ImageIcon(role2.getBodypath()));
		lblNewLabel_1.setBounds(385, 100, 80, 80);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("LV."+role.getLv());
		lblNewLabel_2.setBounds(112, 146, 54, 15);
		panel.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("LV."+role2.getLv());
		lblNewLabel_3.setBounds(411, 75, 54, 15);
		panel.add(lblNewLabel_3);
		
		lblNewLabel_4 = new JLabel("New label");
		lblNewLabel_4.setIcon(new ImageIcon("D:\\eclipse-workspace\\RNGUzi\\src\\Image\\\u80CC\u666F.png"));
		lblNewLabel_4.setBounds(0, 0, 555, 306);
		panel.add(lblNewLabel_4);
		
		Equ();
		
	}
	
	public void initBar() {
		progressBar = new JProgressBar();
		progressBar.setMaximum((int)role.getMaxHp());
		progressBar.setValue((int)role.getMaxHp());
		progressBar.setString(String.valueOf(progressBar.getValue()));
		progressBar.setForeground(Color.RED);
		progressBar.setBackground(Color.WHITE);
		progressBar.setStringPainted(true);
		progressBar.setBounds(382, 243, 146, 14);
		
		progressBar_1 = new JProgressBar();
		progressBar_1.setMaximum((int)role2.getMaxHp());
		progressBar_1.setValue((int)role2.getMaxHp());
		progressBar_1.setString(String.valueOf(progressBar_1.getValue()));
		progressBar_1.setForeground(Color.RED);
		progressBar_1.setStringPainted(true);
		progressBar_1.setBackground(Color.WHITE);
		progressBar_1.setBounds(67, 34, 146, 14);
		
		blueBar = new JProgressBar();
		blueBar.setMaximum((int)role.getMaxMp());
		blueBar.setValue((int)role.getMaxMp());
		blueBar.setString(String.valueOf(blueBar.getValue()));
		blueBar.setStringPainted(true);
		blueBar.setForeground(Color.BLUE);
		blueBar.setBackground(Color.WHITE);
		blueBar.setBounds(382, 261, 146, 14);
		
		blueBar_1 = new JProgressBar();
		blueBar_1.setMaximum((int)role2.getMaxMp());
		blueBar_1.setValue((int)role2.getMaxMp());
		blueBar_1.setString(String.valueOf(blueBar_1.getValue()));
		blueBar_1.setForeground(Color.BLUE);
		blueBar_1.setBackground(Color.WHITE);
		blueBar_1.setStringPainted(true);
		blueBar_1.setBounds(67, 48, 146, 14);
	}

	
	public void Equ() {
		Object[] options = new Object[]{"多兰剑", "多兰盾"};
		k = JOptionPane.showOptionDialog(
				this,
				"请选择装备作战：", 
				"装备选择", 
				JOptionPane.YES_NO_CANCEL_OPTION, 
				JOptionPane.ERROR_MESSAGE, 
				null, 
				options, 
				options[0]);
		if(k >= 0) {
			role.equipment(k);
			if(k == 0) {
				showMessage("选择多兰剑，攻击力加10");
			}else if(k == 1) {
				showMessage("选择多兰盾，防御力加10");
			}
		}
	}
	
	public void refreshHp(Role myrole,Role role,JProgressBar myprogressbar,JProgressBar progressbar,JProgressBar bluebar,int flag,int time) {
		bluebar.setValue((int)myrole.getMp());
		bluebar.setString(String.valueOf(bluebar.getValue()));
		//myprogressbar.setValue((int)myrole.getHp());
		//myprogressbar.setString(String.valueOf(myprogressbar.getValue()));
		
		if(flag != 2) {
			timer.cancel();
			timer = new Timer();
			timer.schedule(new delay_timer(role,progressbar,(int)role.getLastHp(),timer), time, 15);
		}else {
			timer.cancel();
			timer = new Timer();
			timer.schedule(new delay_timer(myrole,myprogressbar,(int)myrole.getLastHp(),timer), time, 15);
		}
	}
	
	public static void showMessage(String str)
	{
		JOptionPane.showMessageDialog(null,str);
	}
	
	public boolean ifWin() throws InterruptedException{
		if(role2.getHp()<=0) {
			showMessage("战斗胜利！掉落"+5*role.getLv()+"经验值!");
			role.setExp(role.getExp()+5*role.getLv());
		}else if(role.getHp()<=0) {
			showMessage("战斗失败！");
		}
		if(role2.getHp()<=0||role.getHp()<=0) {
			role.removeEqu(k);
			backMove();
			return true;
		}
		return false;
	}
	
	public void enemyattack(Role role,Role role2)  {
		int i;
		while(true) {
			i = (int)(Math.random()*8);
			if(i>=0&&i<=3) {
				role.skill1(role2);
				showMessage("敌方使用了"+role.getsName1());
				refreshHp(role,role2,progressBar_1,progressBar,blueBar_1,1,1000);
				break;
			}else if(i==4||i==5) {
				role.skill2();
				if(flag) {
					showMessage("敌方使用了"+role.getsName2());
					refreshHp(role,role2,progressBar_1,progressBar,blueBar_1,2,1000);
					break;
				}else {
					//System.out.println("治疗蓝量不足 重新选择");
					continue;
				}
			}else{
				role.skill3(role2);
				if(flag) {
					showMessage("敌方使用了"+role.getsName3());
					refreshHp(role,role2,progressBar_1,progressBar,blueBar_1,3,1000);
					break;
				}else {
					//System.out.println("大招蓝量不足 重新选择");
					continue;
				}
			}
		}
			
	}
	
	public void attack_course(int i) throws InterruptedException {
		role.attack(i, role2);
		if(flag) {
			refreshHp(role,role2,progressBar,progressBar_1,blueBar,i,0);
			if(ifWin()) {
				return;
			}
			showMessage("敌方回合!");
			
			enemyattack(role2,role);
			if(ifWin()) {
				return;
			}
		}else {
			return;
		}
	}
	
	public void backMove() throws InterruptedException{
		this.dispose();
		new MoveFrame(role);
		new ArcFrame(role).setVisible(true);
	}
	
}
