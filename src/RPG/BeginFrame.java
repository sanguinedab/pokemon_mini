package RPG;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * 
 * @author 蜡笔ab
 *
 */
public class BeginFrame extends JFrame {

	private JPanel contentPane;

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BeginFrame frame = new BeginFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BeginFrame() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(377, 237);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 360, 198);
		panel.setLayout(null);
		contentPane.add(panel);
		
		Au a = new Au();
		a.play(2);
		
		JButton btn = new JButton();
		btn.setIcon(new ImageIcon("src\\Image\\\u65B0\u6E38\u620F.png"));
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				a.stop();
				Au w = new Au();
				w.play(1);
				Hero hero = new Hero();
				try {
					new MoveFrame(hero);
				} catch (InterruptedException e) {
					// TODO 自动生成的 catch 块
					e.printStackTrace();
				}
				dispose();
			}
		});
		panel.setLayout(null);
		btn.setBounds(218, 10, 132, 51);
		btn.setBorderPainted(false);
		btn.setContentAreaFilled(false);
		btn.setFocusPainted(false);
		panel.add(btn);
		
		JButton btn_1 = new JButton();
		btn_1.setIcon(new ImageIcon("src\\Image\\\u7EE7\u7EED\u6E38\u620F.png"));
		btn_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				a.stop();
				Au w = new Au();
				w.play(1);
				Hero hero = new Hero();
				Read r = new Read();
				int[] readFile = r.read();
				hero.setHp(readFile[0]);
				hero.setMaxHp(readFile[0]);
				hero.setMp(readFile[1]);
				hero.setMaxMp(readFile[1]);
				hero.setAtk(readFile[2]);
				hero.setDef(readFile[3]);
				hero.setLv(readFile[4]);
				hero.setExp(readFile[5]);
				try {
					new MoveFrame(hero);
				} catch (InterruptedException e1) {
					// TODO 自动生成的 catch 块
					e1.printStackTrace();
				}
				dispose();
			}
		});
		btn_1.setBounds(218, 71, 132, 50);
		btn_1.setBorderPainted(false);
		btn_1.setContentAreaFilled(false);
		btn_1.setFocusPainted(false);
		panel.add(btn_1);
		
		JLabel lblNewLabel = new JLabel();
		lblNewLabel.setIcon(new ImageIcon("src\\Image\\\u5C01\u9762.gif"));
		lblNewLabel.setBounds(0, 0, 360, 198);
		panel.add(lblNewLabel);

	}
}
