package RPG;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;

public class Pikachu extends Thread {
	static int x = 200,y = 100,width = 50,height = 50;
	static Image image = new ImageIcon("src\\Image\\Ƥ��������.png").getImage();
	static boolean up = false;
	static boolean down = false;
	static boolean left = false;
	static boolean right = false;
	static int towards = 2;
	static int up1 = 0;
	static int down1 = 0;
	static int left1 = 0;
	static int right1 = 0;
	
	public static void Update() {
		x = 200;
		y = 100;
		width = 50;
		height = 50;
		up = false;
		down = false;
		left = false;
		right = false;
	}
	
	public void run() {
		while(!Thread.currentThread().isInterrupted()){
			move();
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				//e.printStackTrace();
			}
		}
	}
	
	public void move() {
		if(up) {
			up1++;
			if(up1 >= 20) {
				up1=0;
			}
			y--;
		}
		if(down) {
			down1++;
			if(down1 >= 20) {
				down1=0;
			}
			y++;
		}
		if(left) {
			left1++;
			if(left1 >= 20) {
				left1=0;
			}
			x--;
		}
		if(right) {
			right1++;
			if(right1 >= 20) {
				right1=0;
			}
			x++;
		}
	}
	
	public static void myDraw(Graphics g) {
		if(!up&&!down&&!left&&!right) {
			if(towards == 1) {
				g.drawImage(Pictures.draw(13).getImage(), x, y, width, height, null);
			}else if(towards == 2) {
				g.drawImage(Pictures.draw(4).getImage(), x, y, width, height, null);
			}else if(towards == 3) {
				g.drawImage(Pictures.draw(7).getImage(), x, y, width, height, null);
			}else if(towards == 4) {
				g.drawImage(Pictures.draw(10).getImage(), x, y, width, height, null);
			}
		}else {
			if(up) {
				if(up1<5) {
					g.drawImage(Pictures.draw(13).getImage(), x, y, width, height, null);
				}else if(up1<10) {
					g.drawImage(Pictures.draw(14).getImage(), x, y, width, height, null);
				}else if(up1<15) {
					g.drawImage(Pictures.draw(13).getImage(), x, y, width, height, null);
				}else if(up1<20) {
					g.drawImage(Pictures.draw(15).getImage(), x, y, width, height, null);
				}
			}else if(down) {
				if(down1<5) {
					g.drawImage(Pictures.draw(4).getImage(), x, y, width, height, null);
				}else if(down1<10) {
					g.drawImage(Pictures.draw(5).getImage(), x, y, width, height, null);
				}else if(down1<15) {
					g.drawImage(Pictures.draw(4).getImage(), x, y, width, height, null);
				}else if(down1<20) {
					g.drawImage(Pictures.draw(6).getImage(), x, y, width, height, null);
				}
			}else if(left) {
				if(left1<5) {
					g.drawImage(Pictures.draw(7).getImage(), x, y, width, height, null);
				}else if(left1<10) {
					g.drawImage(Pictures.draw(8).getImage(), x, y, width, height, null);
				}else if(left1<15) {
					g.drawImage(Pictures.draw(7).getImage(), x, y, width, height, null);
				}else if(left1<20) {
					g.drawImage(Pictures.draw(9).getImage(), x, y, width, height, null);
				}
			}else if(right) {
				if(right1<5) {
					g.drawImage(Pictures.draw(10).getImage(), x, y, width, height, null);
				}else if(right1<10) {
					g.drawImage(Pictures.draw(11).getImage(), x, y, width, height, null);
				}else if(right1<15) {
					g.drawImage(Pictures.draw(10).getImage(), x, y, width, height, null);
				}else if(right1<20) {
					g.drawImage(Pictures.draw(12).getImage(), x, y, width, height, null);
				}
			}
		}
	} 
	
	public int getI() {
		return y/50;
	}
	
	public int getJ() {
		return x/50;
	}
	
	

}
