package RPG;

public class NPC3 extends Role {
	Au a = new Au();

	public NPC3() {
		super("妙蛙花", 200, 200, 120,120, 60, 30,3, "src\\Image\\妙蛙花.png","藤鞭","治疗", "阳光烈焰");
		// TODO 自动生成的构造函数存根
	}

	@Override
	public void attack(int skill, Role role) {
		// TODO 自动生成的方法存根

	}

	@Override
	public void skill1(Role role) {
		// TODO 自动生成的方法存根
		a.play(11);
		role.setLastHp(role.getHp());
		role.setHp(role.getHp()-this.getAtk()+role.getDef());
	}

	@Override
	public void skill2() {
		// TODO 自动生成的方法存根
		this.setLastMp(this.getMp());
		this.setMp(this.getMp()-0.15*this.getMaxMp());
		if(this.getMp() <=0) {
			this.setMp(this.getLastMp());
			StartFrame.flag = false;
			return;
		}else {
			a.play(6);
			this.setLastHp(this.getHp());
			if(this.getHp() <= this.getMaxHp()*0.2) {
				this.setHp(this.getHp()+(this.getMaxHp()-this.getHp())*0.35);
			}else {
				this.setHp(this.getHp()+(this.getMaxHp()-this.getHp())*0.25);
			}
		}
	}

	@Override
	public void skill3(Role role) {
		// TODO 自动生成的方法存根
		this.setLastMp(this.getMp());
		this.setMp(this.getMp()-0.3*this.getMaxMp());
		if(this.getMp() <=0) {
			this.setMp(this.getLastMp());
			StartFrame.flag = false;
			return;
		}else {
			a.play(12);
			role.setLastHp(role.getHp());
			role.setHp(role.getHp()-this.getAtk()*1.3+role.getDef());
		}
	}

	@Override
	public void equipment(int i) {
		// TODO 自动生成的方法存根

	}

	@Override
	public void levelup() {
		// TODO 自动生成的方法存根

	}

	@Override
	public void removeEqu(int i) {
		// TODO 自动生成的方法存根
		
	}

}
