package RPG;

import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JProgressBar;

public class delay_timer extends TimerTask {
	Role role;
	JProgressBar progressBar;
	int i;
	Timer timer;

	public delay_timer(Role role, JProgressBar progressBar, int i,Timer timer) {
		super();
		this.role = role;
		this.progressBar = progressBar;
		this.i=i;
		this.timer = timer;
	}

	@Override
	public void run() {
		// TODO 自动生成的方法存根
		if(i>(int)role.getHp()) {
			i--;
		}else if(i<(int)role.getHp()) {
			i++;
		}
		if(i>role.getHp() || i<role.getHp()){
			progressBar.setValue((int)i);
			progressBar.setString(String.valueOf(progressBar.getValue()));
		}else {
			progressBar.setValue((int)role.getHp());
			progressBar.setString(String.valueOf(progressBar.getValue()));
			timer.cancel();
		}
	}

}
