package RPG;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * 
 * @author 蜡笔ab
 *
 */
public class MoveFrame extends JFrame {
	Role hero;
	private JPanel contentPane;
	NPC1 npc1 = new NPC1();
	NPC2 npc2 = new NPC2();
	NPC3 npc3 = new NPC3();
	Rectangle dragonRec = new Rectangle(0,0,100,100);
	Rectangle waterRec = new Rectangle(350,0,150,50);
	Rectangle frogRec = new Rectangle(400,150,50,50);
	Rectangle pikaRec;
	Pikachu pika;
	UpdateThread ut;
	static int map1[][] = new int[6][10];


	/**
	 * Create the frame.
	 * @throws InterruptedException 
	 * 
	 */
	public MoveFrame(Role hero) throws InterruptedException{
		this.hero = hero;
		hero.levelup();
		Pikachu.Update();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(500, 300);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new 	MyPanel();
		panel.setBounds(0, 0, 484, 261);
		contentPane.add(panel);
		panel.setLayout(null);
		
		
		
		this.setVisible(true);
		this.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO 自动生成的方法存根
				if(KeyEvent.VK_UP == e.getKeyCode()) {
					Pikachu.towards = 1;
					Pikachu.up = true;
				}else if(KeyEvent.VK_DOWN == e.getKeyCode()) {
					Pikachu.towards = 2;
					Pikachu.down = true;
				}else if(KeyEvent.VK_LEFT == e.getKeyCode()) {
					Pikachu.towards = 3;
					Pikachu.left = true;
				}else if(KeyEvent.VK_RIGHT == e.getKeyCode()) {
					Pikachu.towards = 4;
					Pikachu.right = true;
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO 自动生成的方法存根
				if(KeyEvent.VK_UP == e.getKeyCode()) {
					Pikachu.up = false;
				}else if(KeyEvent.VK_DOWN == e.getKeyCode()) {
					Pikachu.down = false;
				}else if(KeyEvent.VK_LEFT == e.getKeyCode()) {
					Pikachu.left = false;
				}else if(KeyEvent.VK_RIGHT == e.getKeyCode()) {
					Pikachu.right = false;
				}
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO 自动生成的方法存根
				
			}
		});
		pika = new Pikachu();
		pika.start();
		ut = new UpdateThread(panel);
		ut.start();
	}
	
	public void openStart(int i) {
		pika.interrupt();
		//ut.interrupt();
		if(i == 1) {
			StartFrame sf = new StartFrame(hero,npc1);
			sf.setVisible(true);
		}else if(i == 2) {
			StartFrame sf = new StartFrame(hero,npc2);
			sf.setVisible(true);
		}else if(i == 3) {
			StartFrame sf = new StartFrame(hero,npc3);
			sf.setVisible(true);
		}
		this.dispose();
	}
	
	public boolean collision() {
		pikaRec = new Rectangle(Pikachu.x,Pikachu.y,Pikachu.width,Pikachu.height);
		if(dragonRec.intersects(pikaRec)) {
			openStart(1);
			return true;
		}else if(waterRec.intersects(pikaRec)) {
			openStart(2);
			return true;
		}else if(frogRec.intersects(pikaRec)) {
			openStart(3);
			return true;
		}
		return false;
	}
	
	class MyPanel extends JPanel {
		public void paint(Graphics g) {
			super.paint(g);
			for(int i=0;i<6;i++)
				for(int j=0;j<10;j++) {
					if(i == 2&&j == 2) {
						g.drawImage(Pictures.draw(0).getImage(), 50*j, 50*i, 50, 50, null);
						g.drawImage(Pictures.draw(22).getImage(), 50*j, 50*i, 50, 50, null);
					}else if(i <= 2&&j == 2) {
						g.drawImage(Pictures.draw(0).getImage(), 50*j, 50*i, 50, 50, null);
						g.drawImage(Pictures.draw(23).getImage(), 50*j, 50*i, 50, 50, null);
					}else if(i == 2&&j <= 2) {
						g.drawImage(Pictures.draw(0).getImage(), 50*j, 50*i, 50, 50, null);
						g.drawImage(Pictures.draw(24).getImage(), 50*j, 50*i, 50, 50, null);
					}else if(i < 2&&j < 2) {
						g.drawImage(Pictures.draw(25).getImage(), 50*j, 50*i, 50, 50, null);
					}else if(i == 1&&j == 6) {
						g.drawImage(Pictures.draw(0).getImage(), 50*j, 50*i, 50, 50, null);
						g.drawImage(Pictures.draw(17).getImage(), 50*j, 50*i, 50, 50, null);
					}else if(i <= 1&&j == 6) {
						g.drawImage(Pictures.draw(0).getImage(), 50*j, 50*i, 50, 50, null);
						g.drawImage(Pictures.draw(18).getImage(), 50*j, 50*i, 50, 50, null);
					}else if(i == 1&&j >= 6) {
						g.drawImage(Pictures.draw(0).getImage(), 50*j, 50*i, 50, 50, null);
						g.drawImage(Pictures.draw(19).getImage(), 50*j, 50*i, 50, 50, null);
					}else if(i < 1&&j > 6) {
						g.drawImage(Pictures.draw(20).getImage(), 50*j, 50*i, 50, 50, null);
					}else {
						g.drawImage(Pictures.draw(0).getImage(), 50*j, 50*i, 50, 50, null);
					}
				}
			
			Pikachu.myDraw(g);
			
			for(int i=3;i<6;i++)
				for(int j=0;j<10;j++) {
					if((i == 3&&j == 9)||(i == 4&&j>=5)) {
						g.drawImage(Pictures.draw(21).getImage(), 50*j, 50*i, 50, 50, null);
						}
				}
			
			g.drawImage(Pictures.draw(1).getImage(),50,0,50,50,null);
			
			g.drawImage(Pictures.draw(2).getImage(),350,0,50,50,null);
			
			g.drawImage(Pictures.draw(3).getImage(),400,150,50,50,null);
		}

	}
	
	class UpdateThread extends Thread {
		JPanel panel;
		public UpdateThread(JPanel panel) {
			this.panel = panel;
		}
		
		@Override
		public void run() {
			while(!Thread.currentThread().isInterrupted()){
				panel.repaint();
				if(collision()) {
					break;
				}
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

	}
}
