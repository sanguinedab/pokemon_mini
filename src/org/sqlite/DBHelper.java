package org.sqlite;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class DBHelper {
	static Connection con=null;
	static Statement s=null;
	static PreparedStatement ps=null;
	static ResultSet rs=null,rs1=null;
    public void connectDB(String path){
            try {
                Class.forName("org.sqlite.JDBC");
                con=DriverManager.getConnection(path);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }

    }
    
    public ResultSet executeQuery(String sql) throws SQLException
    {
        s = con.createStatement();
        return s.executeQuery(sql);

    }
    
    public ResultSet executeQuery(String sql, String[] strs)throws SQLException 
    {
			ps = con.prepareStatement(sql);
			setParams(ps,strs);
			return rs = ps.executeQuery();
    }
    
    public void executeUpdate(String sql)
    {
        try {
            s = con.createStatement();
            s.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void executeUpdate(String sql, String[] strs)
    {
    	try
		{			
			ps = con.prepareStatement(sql);
			setParams(ps, strs);
			rs = ps.executeQuery(); 
		} catch (SQLException sqle)
		{
			sqle.printStackTrace();
		} 
    }
    
    public void closeConnection(){
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setParams(PreparedStatement ps, String[] strs) throws SQLException
	{
		if (strs != null && strs.length > 0)
		{
			for (int i = 0; i < strs.length; i++)
			{
				ps.setObject(i + 1, strs.length);
			}
		}
	}

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		DBHelper dh=new DBHelper();
		String path="jdbc:sqlite://F:/stuDB.db";
        dh.connectDB(path);

        try {
        	ResultSet rs=dh.executeQuery("select * from W");
            while(rs.next()){
                int id=rs.getInt("身份证");
                String name=rs.getString("用户名");
                System.out.println(id+name);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        dh.closeConnection();
        System.out.println("OK");
	}

}
